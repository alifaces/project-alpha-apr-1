from django.urls import path
from tasks.views import task_create, task_show


urlpatterns = [
    path("create/", task_create, name="create_task"),
    path("mine/", task_show, name="show_my_tasks"),
]
